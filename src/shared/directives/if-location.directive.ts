import {AfterViewInit, Directive, Input, OnDestroy, TemplateRef, ViewContainerRef} from '@angular/core';
import { ActivatedUser } from '../../auth/services/activated-user.service';
import { AuthGuard } from '../../auth/guards/auth.guard';
import { Subscription } from 'rxjs';

// tslint:disable directive-selector
@Directive({
    selector: '[if-location]'
})
export class IfLocationDirective implements AfterViewInit, OnDestroy {

    private $implicit: string;
    subscription: Subscription;

    constructor(private activateUser: ActivatedUser,
        private view: ViewContainerRef,
        private authGuard: AuthGuard,
        private template: TemplateRef<any>) {
        
    }
    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    @Input('if-location') set assign(value: string) {
        if (value) {
            Object.assign(this, {
                $implicit: value
            });
        }
    }

    public get value() {
        return this.$implicit;
    }

    public set value(value: string) {
        this.$implicit = value;
    }

    ngAfterViewInit(): void {
        this.subscription = this.activateUser.user.subscribe((user) => {
            const locationPermission = this.authGuard.canActivateLocation(this.$implicit, user);
            const mask = locationPermission && locationPermission.mask;
            if ((mask & 1) === 0) {
                this.view.clear();
            } else {
                this.view.createEmbeddedView(this.template);
            }
        });
    }

}
// tslint:enable directive-selector
