import {Pipe, PipeTransform} from '@angular/core';
import {template, TemplateSettings} from 'lodash';

/**
 *  Template Pipe is an impure pipe that can escape
 *  HTML and interpolated data properties and
 *  execute JavaScript in "evaluate" delimiters
 */
@Pipe({
  name: 'template',
  pure: false
})
export class TemplatePipe implements PipeTransform {
  constructor() {
  }

  /**
   * Uses lodash template function to generate a string
   * from a given template escaping the delimiters. It
   * deletes the template source after its initialization,
   * generates the string and nullifies the compiled template
   * function variable to be garbage-collected.
   * @param {string} inputTemplate: The template used to generate the string
   * @param {any} value: The parameter passed to compiled template function
   * @param {Object | undefined} options: Custom options to pass to lodash template
   */
  transform(inputTemplate: string, value: any, options?: TemplateSettings | undefined): any {
    let _tpl;
    if (options !== undefined) {
      /*
       lodash doesn't throw error when options are not valid, so
       there is not need to add a try catch block
       */
      _tpl = template(inputTemplate, options);
    } else {
      _tpl = template(inputTemplate);
    }
    delete _tpl.source;
    const _res = _tpl(value);
    _tpl = null;
    return _res;
  }
}
