import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService } from '../../shared/services/configuration.service';

/**
 * @private
 */
const currentProfileProperty = 'currentProfile';
/**
 * @private
 */
const currentUserProperty = 'currentUser';

declare var $: any;

@Injectable()
export class UserService {


  private _user: any;


  constructor(private _context: AngularDataContext,
    private _configurationService: ConfigurationService) {
  }

  /**
   * Refreshes user token taken by implicit authorization flow.
   */
  checkLogin() {
    // set refresh frame
    const settings = this._configurationService.settings.auth;
    const _iframe = $('<iframe>', {
      /* tslint:disable max-line-length */
      src: `${settings.authorizeURL}?response_type=token&client_id=${settings.oauth2.clientID}&redirect_uri=${settings.oauth2.callbackURL}&prompt=none`,
      id: 'openid',
      frameborder: 1,
      scrolling: 'no',
      onload: function() {
        //
      }
    }).hide().appendTo('body');
  }


  /**
   * Gets current user
   * @returns {Promise<*>} An object which represents the current user
   *
   * Example
   * ```typescript
import {UserService} from '@universis/common';

constructor(private _userService: UserService) {
  //
}
ngOnInit() {
  this._userService.getUser().then(user => {
    // place code here...
  });
}
```
   */
  async getUser(): Promise<any> {
    return this.getUserSync();
  }

  getUserSync() {
    // if user already taken
    if (this._user) {
      // return user
      return this._user;
    }
    if (sessionStorage.getItem(currentUserProperty)) {
      // get user from storage
      const currentUser = JSON.parse(sessionStorage.getItem(currentUserProperty));
      // get interactive user
      this._user = Object.assign({}, currentUser);
      // set context
      Object.defineProperty(this._user, 'context', {
        configurable: true,
        enumerable: false,
        value: this._context
      });
      // set user bearer authorization
      this._context.setBearerAuthorization(this._user.token.access_token);
      // set current language
      this._context.getService().setHeader('accept-language', this._configurationService.currentLocale);
      // return user
      return this._user;
    }
    return null;
  }

  /**
   * Sets current user profile
   * @param {*} profile
   * @returns UserService
   */
  setProfile(profile?: any): UserService {
    if (typeof profile === 'undefined' || profile == null) {
      sessionStorage.removeItem(currentProfileProperty);
    } else {
      sessionStorage.setItem(currentProfileProperty, JSON.stringify(profile));
    }
    return this;
  }

}
